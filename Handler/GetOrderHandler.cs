﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VishalLodhiya_Milestone2Codingtest_September9_300.Commands;
using VishalLodhiya_Milestone2Codingtest_September9_300.Models;
using VishalLodhiya_Milestone2Codingtest_September9_300.Persistence;

namespace VishalLodhiya_Milestone2Codingtest_September9_300.Handler
{
    public class AddOrderHandler : IRequestHandler<AddOrder, ProductOrder>
    {
        private readonly IGrocery _data;

        public AddOrderHandler(IGrocery data)
        {
            _data = data;
        }

        public Task<ProductOrder> Handle(AddOrder request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.addingOrder(request.ProductId, request.UserId));
        }
    }
}

