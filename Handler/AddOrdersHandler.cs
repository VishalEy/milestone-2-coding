﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VishalLodhiya_Milestone2Codingtest_September9_300.Commands;
using VishalLodhiya_Milestone2Codingtest_September9_300.Models;
using VishalLodhiya_Milestone2Codingtest_September9_300.Persistence;

namespace VishalLodhiya_Milestone2Codingtest_September9_300.Handler
{
    public class AddOrdersHandler : IRequestHandler<AddOrder, ProductOrder>
    {
        private readonly IGrocery _data;

        public AddOrdersHandler(IGrocery data)
        {
            _data = data;
        }

        public async Task<ProductOrder> Handle(AddOrder request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.addingOrder(request.UserId, request.ProductId));
        }
    }
}
