﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VishalLodhiya_Milestone2Codingtest_September9_300.Models;
using VishalLodhiya_Milestone2Codingtest_September9_300.Persistence;

using VishalLodhiya_Milestone2Codingtest_September9_300.Queries;

namespace VishalLodhiya_Milestone2Codingtest_September9_300.Handler
{

    public class getCategoriesHandler : IRequestHandler<GetCategory,IEnumerable<Category>>
    {
        private readonly IGrocery _data;

        public getCategoriesHandler(IGrocery data)
        {
            _data = data;
        }

        

        public async Task<IEnumerable<Category>> Handle(GetCategory request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.getCategories());
        }
    }
}
