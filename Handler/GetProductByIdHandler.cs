﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VishalLodhiya_Milestone2Codingtest_September9_300.Models;
using VishalLodhiya_Milestone2Codingtest_September9_300.Persistence;
using VishalLodhiya_Milestone2Codingtest_September9_300.Queries;

namespace VishalLodhiya_Milestone2Codingtest_September9_300.Handler
{
    public class GetProductByIdHandler : IRequestHandler<getProductById, ProductOrder>
    {
        private readonly IGrocery _data;

        public GetProductByIdHandler(IGrocery data)
        {
            _data = data;
        }

        public Task<ProductOrder> Handle(getProductById request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_data.getProducts(request.Productid));
        }
    }
}
