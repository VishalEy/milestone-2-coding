﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VishalLodhiya_Milestone2Codingtest_September9_300.Commands;
using VishalLodhiya_Milestone2Codingtest_September9_300.Models;
using VishalLodhiya_Milestone2Codingtest_September9_300.Queries;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VishalLodhiya_Milestone2Codingtest_September9_300.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IMediator _mediatr;

        public HomeController(IMediator mediatr)
        {
            _mediatr = mediatr;
        }




        // GET: api/<HomeController>
        [HttpGet]
        [Route("getProducts")]
        public async Task<IEnumerable<Product>> Get()
        {
            return await _mediatr.Send(new getProducts());
        }

        [HttpGet]
        [Route("getCategories")]
        public async Task<IEnumerable<Category>> GetCategories()
        {
            return await _mediatr.Send(new getCategory());
        }

        // GET api/<HomeController>/5
        [HttpGet]
        [Route("getProductById")]
        public async Task<Product> Get(int id)
        {
            return await _mediatr.Send(new getProductById() { Productid = id });
        }

        [HttpGet]
        [Route("getOrder")]
        public async Task<IEnumerable<ProductOrder>> GetOrder(int id1)
        {
            return await _mediatr.Send(new getOrder() { Userid = id1 });
        }

        // POST api/<HomeController>
        [HttpPost]
        public async Task<ProductOrder> Post(int userid, int productid)
        {
            return await _mediatr.Send(new AddOrder() { ProductId = productid, UserId = userid });
        }
    }
}
