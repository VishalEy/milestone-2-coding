﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VishalLodhiya_Milestone2Codingtest_September9_300.Models;

namespace VishalLodhiya_Milestone2Codingtest_September9_300.Commands
{
    public class AddOrder : IRequest<ProductOrder>
    {
        public int ProductId { get; set; }
        public int UserId { get; set; }
    }
}
