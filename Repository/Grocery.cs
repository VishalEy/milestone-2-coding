﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VishalLodhiya_Milestone2Codingtest_September9_300.Models;
using VishalLodhiya_Milestone2Codingtest_September9_300.Persistence;

namespace VishalLodhiya_Milestone2Codingtest_September9_300.Repository
{
    public class Grocery : IGrocery
    {
        private readonly GroceryDBContext _data;

        public Grocery(GroceryDBContext data)
        {
            _data = data;
        }

        public ProductOrder addingOrder(int productid, int userid)
        {
            var orderid = _data.ProductOrder.Max(x => x.Orderid) + 1;
            var product = _data.Product.SingleOrDefault(x => x.Productid == productid);
            var user = _data.ApplicationUser.SingleOrDefault(x => x.Id == userid);
            var productorder = new ProductOrder()
            {
                Userid = userid,
                Productid = productid,
                Orderid = orderid,
                Product = product,
                User = user
            };
            _data.ProductOrder.Add(productorder);
            _data.SaveChanges();
            return productorder;
        }

        public IEnumerable<Category> getCategories()
        {
            return _data.Category.ToList();
        }

        public IEnumerable<ProductOrder> getOrder(int Userid)
        {
            return _data.ProductOrder.Where(o => o.Userid == Userid).ToList();
        }

        public IEnumerable<ProductOrder> getProducts()
        {
            return _data.Product.ToList();
        }

        public ProductOrder getProducts(int Productid)
        {
            var prod = _data.Product.FirstOrDefault((System.Linq.Expressions.Expression<Func<ProductOrder, bool>>)(o => o.Productid == Productid));
            return prod;
        }
    }
}


