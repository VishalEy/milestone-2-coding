﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VishalLodhiya_Milestone2Codingtest_September9_300.Models;

namespace VishalLodhiya_Milestone2Codingtest_September9_300.Queries
{
    public class getProductById : IRequest<ProductOrder>
    {
        public int Productid { get; set; }
    }
    
    }

