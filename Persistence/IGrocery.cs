﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VishalLodhiya_Milestone2Codingtest_September9_300.Models;

namespace VishalLodhiya_Milestone2Codingtest_September9_300.Persistence
{
    public interface IGrocery
    {
        public IEnumerable<ProductOrder> getProducts();
        public IEnumerable<Category> getCategories();
        public ProductOrder addingOrder(int userid, int productid);
        public ProductOrder getProducts(int Productid);
        public IEnumerable<ProductOrder> getOrder(int Userid);
    }
}
