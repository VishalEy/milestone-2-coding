﻿using System;
using System.Collections.Generic;

namespace VishalLodhiya_Milestone2Codingtest_September9_300.Models
{
    public partial class ProductOrder
    {
        public int Orderid { get; set; }
        public int Productid { get; set; }
        public int Userid { get; set; }

        public virtual ProductOrder Product { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}
