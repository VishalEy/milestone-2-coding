﻿using System;
using System.Collections.Generic;

namespace VishalLodhiya_Milestone2Codingtest_September9_300.Models
{
    public partial class MenuBar
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public bool? Openinnewwindow { get; set; }
    }
}
