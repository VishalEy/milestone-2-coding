﻿using System;
using System.Collections.Generic;

namespace VishalLodhiya_Milestone2Codingtest_September9_300.Models
{
    public partial class Category
    {
        public int Catid { get; set; }
        public string CategoryName { get; set; }
    }
}
